package nirvana.client.zk;

/**
 * 客户端在ZooKeeper注册的属性
 * @author zhanghongyu
 */
public class ZooKeeperInfoBean {
	/**
	 * 在线时长
	 */
	private String up_secs;
	/**
	 * 主机地址
	 */
	private String host_name;
	/**
	 * 最后一次心跳的时间
	 */
	private String time_secs;

	public String getUp_secs() {
		return up_secs;
	}

	public void setUp_secs(String up_secs) {
		this.up_secs = up_secs;
	}

	public String getHost_name() {
		return host_name;
	}

	public void setHost_name(String host_name) {
		this.host_name = host_name;
	}

	public String getTime_secs() {
		return time_secs;
	}

	public void setTime_secs(String time_secs) {
		this.time_secs = time_secs;
	}
	
	
}
