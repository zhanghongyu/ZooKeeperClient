package nirvana.client.zk;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
/**
 * 检验工具类
 * @author zhanghongyu
 */
public class CheckUtil {
	public static boolean isEmpty(Collection<?> collection) {
		if (collection == null) {
			return true;
		} else {
			return collection.isEmpty();
		}
	}

	public static boolean isNotEmpty(Collection<?> collection) {
		return !isEmpty(collection);
	}

	public static boolean isEmpty(Map<?, ?> map) {
		if (map == null) {
			return true;
		} else {
			return map.isEmpty();
		}
	}

	public static boolean isNotEmpty(Map<?, ?> map) {
		return !isEmpty(map);
	}

	public static boolean isNull(Object object) {
		if (object == null) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isNotNull(Object object) {
		return !isNull(object);
	}

	public static boolean isEmpty(Object[] objects) {
		if (objects == null) {
			return true;
		} else if (objects.length == 0) {
			return true;
		}
		return false;
	}

	public static boolean isNotEmpty(Object[] objects) {
		return !isEmpty(objects);
	}

	public static void checkNull(Object object) {
		if (object == null) {
			throw new RuntimeException("object is null");
		}
	}

	public static void checkEmptyString(String string) {
		if (StringUtils.isEmpty(string)) {
			throw new RuntimeException("string is null or empty");
		}
	}

	public static void checkEmptyMap(Map<?, ?> map) {
		if (isEmpty(map)) {
			throw new RuntimeException("map is null or empty");
		}
	}

	public static void checkEmptyCollection(Collection<?> collection) {
		if (isEmpty(collection)) {
			throw new RuntimeException("collection is null or empty");
		}
	}

	public static void checkEmptyArray(Object[] objects) {
		if (isEmpty(objects)) {
			throw new RuntimeException("objects is null or empty");
		}
	}

	public static void checkZero(int i) {
		if (i <= 0) {
			throw new RuntimeException(i + " less than zero");
		}
	}

	public static void checkZero(long l) {
		if (l <= 0L) {
			throw new RuntimeException(l + " less than zero");
		}
	}

	private CheckUtil() {
	
	}
}
