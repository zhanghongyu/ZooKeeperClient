package nirvana.client.zk;
/**
 * ZooKeeper常量类
 * @author zhanghongyu
 */
public class ZooKeeperConstant {
	
	/**
	 *  zookeeper 连接的端口号：10.1.6.106:3333
	 */
	public static final String ZOOKEEPER_HOSTS="10.1.6.106:3333";
	/**
	 *  zookeeper 管理节点的心跳目录：/nirvana/heartbeats
	 */
	public static final String ZOOKEEPER_HEARTBEATS="/nirvana/heartbeats";
	/**
	 *  客户端和服务端心跳的毫秒数: 10000 即10秒
	 */
	public static final int ZOOKEEPER_SESSION_OUT = 10000;
	/**
	 * 在线时长
	 */
	public static final String UP_SECS = "up_secs";
	
	/**
	 * 客户端IP或主机名
	 */
	public static final String HOST_NAME = "host_name";
	/**
	 * 最后心跳时间格式 yyyy-MM-dd hh:mm:ss
	 */
	public static final String TIME_SECS = "time_secs";
}
