package nirvana.client.zk;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.sf.json.JSONObject;
import org.apache.zookeeper.ZooKeeper;
/**
 *  获取客户端在ZooKeeper注册的信息列表
 * @author zhanghongyu
 */
public class ZooKeeperClient {
	/**
	 *  返回客户端在服务端注册的信息: 
	 *               1、在线时长 2、客户端IP或主机名 3、最后的心跳时间（即客户端和服务端的最后通信时间）
	 *  静态方法加锁，其实是锁在了该方法所在类-所对应的Class对象上
	 * @return rtnList
	 * @throws ZooKeeperException
	 */
	public static List<ZooKeeperInfoBean> getZooKeeperClientInfoList() throws ZooKeeperException{
		List<ZooKeeperInfoBean> rtnList = null;
		synchronized (ZooKeeperClient.class) {
			ZooKeeper zooKeeper = ZooKeeperPool.getZooKeeperConnection();
			if(CheckUtil.isNull(zooKeeper)){
				return null;
			}
			List<String> listPaths;
			try {
				listPaths = zooKeeper.getChildren(ZooKeeperConstant.ZOOKEEPER_HEARTBEATS, false);
				if(CheckUtil.isNotEmpty(listPaths)){
					rtnList = new ArrayList<ZooKeeperInfoBean>();
					byte[] by;
					String temPath;
					for(String path:listPaths){
						temPath = ZooKeeperConstant.ZOOKEEPER_HEARTBEATS+"/"+path;
						if(CheckUtil.isNotNull(zooKeeper.exists(temPath, false))){
							by = zooKeeper.getData(temPath, false, null);
							String str = new String(by,"utf-8");
							rtnList.add(getBeanFormJson(str));
						}
					}
				}
			} catch (Exception e) {
				throw new ZooKeeperException(e);
			}
		}
		return rtnList;
	}
	/**
	 * Json串解析，返回实体bean
	 * @param str
	 * @return ZooKeeperInfoBean
	 * @throws ZooKeeperException
	 */
	private static ZooKeeperInfoBean getBeanFormJson(String str) throws ZooKeeperException{
		ZooKeeperInfoBean bean;
		JSONObject obj;
		try {
			obj = JSONObject.fromObject(str);
			bean = new ZooKeeperInfoBean();
			bean.setUp_secs(getHSM(Long.valueOf(obj.get(ZooKeeperConstant.UP_SECS).toString())));
			bean.setHost_name(obj.get(ZooKeeperConstant.HOST_NAME).toString());
			bean.setTime_secs(getTime(Long.valueOf(obj.get(ZooKeeperConstant.TIME_SECS).toString())));
		} catch (Exception e) {
			throw new ZooKeeperException(e);
		} 
		return bean;
	}
	/**
	 *  获取客户端和ZooKeeper最后心跳时间
	 * @param l
	 * @return yyyy-MM-dd hh:mm:ss
	 */
	private static String getTime(long l){
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String str = sdf.format(date);
		return str;
	}
	/**
	 *  获取在线时长
	 * @param l
	 * @return XX小时XX分XX秒
	 */
	private static String getHSM(long l){
		long hour =  l / (60*60*1000);
		long minute = (l - (hour*60*60*1000))/(60*1000); 
		long second = (l - hour*60*60*1000 - minute*60*1000)/1000;
		
		if(second >= 60 ) { 
			second = second % 60; 
			minute+=second/60; 
		} 
		if(minute >= 60) { 
			minute = minute %60; 
			hour += minute/60; 
		}
		String sh = "";
	    String sm ="";
	    String ss = "";
	    if(hour <10) {
	    	sh = "0" + String.valueOf(hour);
	    }else {
	    	sh = String.valueOf(hour);
	    }
	    if(minute <10) {
	    	sm = "0" + String.valueOf(minute);
	    }else {
	    	sm = String.valueOf(minute);
	    }
	    if(second <10) {
	    	ss = "0" + String.valueOf(second);
	    }else {
	    	ss = String.valueOf(second);
	    }
		return sh+"hh"+sm+"mm"+ss+"ss";
	}
}
