package nirvana.client.zk;
/**
 * 异常类封装
 * @author zhanghongyu
 */
public class ZooKeeperException extends Exception {
	
	private static final long serialVersionUID = -4010507888347211595L;

	public ZooKeeperException() {
	
	}

	public ZooKeeperException(String message) {
		super(message);
	}

	public ZooKeeperException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public ZooKeeperException(Throwable throwable) {
		super(throwable);
	}
}
